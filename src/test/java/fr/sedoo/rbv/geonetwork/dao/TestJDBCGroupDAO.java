package fr.sedoo.rbv.geonetwork.dao;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.commons.spring.SpringBeanFactory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfig.xml"})
public class TestJDBCGroupDAO {
	
	@Test
	public void testGetGroups()
	{
		
		List<GeonetworkGroup> groups = getGroupDAO().getGroups();
		Assert.assertNotNull(groups);
		Assert.assertTrue("La liste ne doit pas être vide", groups.size()>0);
	}
	
	@Test
	public void testGetGroupByName()
	{
		GeonetworkGroup aux = getGroupDAO().getGroupByName(GroupDAO.ALL_GROUP_NAME);
		Assert.assertNotNull(aux);
		Assert.assertNotNull(aux.getName());
		Assert.assertTrue("Les noms doivent correspondre", aux.getName().compareToIgnoreCase(GroupDAO.ALL_GROUP_NAME)==0);
	}
	
	
	private GroupDAO getGroupDAO()
	{
		SpringBeanFactory springBeanFactory = new SpringBeanFactory();
		return (GroupDAO) springBeanFactory.getBeanByName(GroupDAO.BEAN_NAME);
	}

}
