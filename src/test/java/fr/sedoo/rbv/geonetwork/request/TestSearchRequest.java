package fr.sedoo.rbv.geonetwork.request;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Assert;

import org.geotoolkit.metadata.iso.extent.DefaultGeographicBoundingBox;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfig.xml"})
public class TestSearchRequest extends AbstractAuthenticatedTest 
{
	GeonetworkUser bvetUser;
	
	
	@Test
	public void testCritereGeographiqueSeul() throws Exception 
	{
		SearchCriteria criteria = new SearchCriteria();
		
		DefaultGeographicBoundingBox boundingBox = new DefaultGeographicBoundingBox();
		boundingBox.setNorthBoundLatitude(5.0);
		boundingBox.setSouthBoundLatitude(1.0);
		boundingBox.setEastBoundLongitude(10.0);
		boundingBox.setWestBoundLongitude(20);
		
		SearchRequest request = new SearchRequest();
		criteria.setBoundingBox(boundingBox);
		request.setCriteria(criteria);
				
		Boolean isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		List<Summary> summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertFalse("La liste ne doit pas être vide", summaries.isEmpty());
		Assert.assertEquals("La liste doit contenir 1 élément", 1, summaries.size());
		
	}
	
	@Test
	public void testObservatoiresSeuls() throws Exception 
	{
		SearchCriteria criteria = new SearchCriteria();
	
		SearchRequest request = new SearchRequest();
		criteria.setIncludesObservatories(true);
		request.setCriteria(criteria);
				
		Boolean isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		List<Summary> summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertFalse("La liste ne doit pas être vide", summaries.isEmpty());
		
	}
	
	
	@Test
	public void testCritereTemporelSeul() throws Exception 
	{
		SearchCriteria criteria = new SearchCriteria();
		
		criteria.setStartDate("2000-01-17");
		criteria.setEndDate("2010-01-17");
		List<String> motcles = new ArrayList<String>();
		motcles.add("BvEt");
		criteria.setKeywords(motcles);
		
		SearchRequest request = new SearchRequest();
		request.setCriteria(criteria);
				
		Boolean isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		List<Summary> summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertFalse("La liste ne doit pas être vide", summaries.isEmpty());
		Assert.assertEquals("La liste doit contenir 14 élément", 14, summaries.size());
		
	}
	
	@Test
	public void testConjonction() throws Exception 
	{
		SearchCriteria criteria = new SearchCriteria();
		
		DefaultGeographicBoundingBox boundingBox = new DefaultGeographicBoundingBox();
		boundingBox.setNorthBoundLatitude(5.0);
		boundingBox.setSouthBoundLatitude(1.0);
		boundingBox.setEastBoundLongitude(10.0);
		boundingBox.setWestBoundLongitude(20);
		
		
		
		criteria.setKeywordsLogicalLink(SearchCriteria.OR);
		List<String> motcles = new ArrayList<String>();
		motcles.add("Hybam");
		motcles.add("IpSuM");
		criteria.setKeywords(motcles);
		SearchRequest request = new SearchRequest();
		criteria.setBoundingBox(boundingBox);
		request.setCriteria(criteria);
				
		Boolean isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		List<Summary> summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertTrue("La liste doit être vide", summaries.isEmpty());
		
		
		boundingBox.setNorthBoundLatitude(21.0);
		boundingBox.setSouthBoundLatitude(-58.0);
		boundingBox.setEastBoundLongitude(-22.0);
		boundingBox.setWestBoundLongitude(-86);
		criteria.setBoundingBox(boundingBox);
		request.setCriteria(criteria);
		
		isLoaded = request.fetchSummaries();
		summaries = request.getSummaries();
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertFalse("La liste ne doit pas être vide", summaries.isEmpty());
	}
	
	@Test
	public void testCritereMotsClefsSeul() throws Exception 
	{
		SearchCriteria criteria = new SearchCriteria();
		
		List<String> motcles = new ArrayList<String>();
		// Requete initiale sur un mot clef existant forcément
		motcles.add("ipsum");
		
		SearchRequest request = new SearchRequest();
		criteria.setKeywords(motcles);
		criteria.setKeywordsLogicalLink(SearchCriteria.AND);
		request.setCriteria(criteria);
		Boolean isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		List<Summary> summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertFalse("La liste ne doit pas être vide", summaries.isEmpty());
		Assert.assertTrue("La liste doit contenir plus de 1 élément", summaries.size()>1);
		
		int initialSize = summaries.size();
		
		//On ajoute un mot clef n'existant pas
		motcles.add("xdddk");
		request.setCriteria(criteria);
		isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertTrue("La liste doit être vide", summaries.isEmpty());
		
		//On bascule le lien à Or
		criteria.setKeywordsLogicalLink(SearchCriteria.OR);
		request.setCriteria(criteria);
		isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertTrue("La liste doit avoir la même taille qu'au début", summaries.size() == initialSize);
		
		motcles.clear();
		criteria.setKeywordsLogicalLink(SearchCriteria.OR);
		motcles.add("BvEt");
		motcles.add("IpSuM");
		request.setCriteria(criteria);
		isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		summaries = request.getSummaries();
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertEquals("La liste doit contenir 3 éléments", 3, summaries.size());
		
		motcles.clear();
		criteria.setKeywordsLogicalLink(SearchCriteria.AND);
		motcles.add("BvEt");
		motcles.add("cameroon");
		request.setCriteria(criteria);
		isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		summaries = request.getSummaries();
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertEquals("La liste doit contenir 1 élément", 1, summaries.size());
		
	}
	
	@Test
	public void testPagination() throws Exception 
	{
		
		String s = "TOTO id=\"019053695\" type=\"B\" la=\" \" status=\"1\" c=\"HG\" date=\"20071120\"";
		 
		Pattern pattern = Pattern.compile("([a-zA-Z_0-9]+)=\"(.*?)\"");
		Matcher matcher = pattern.matcher(s);
		while(matcher.find()) {
			String name = matcher.group(1);
			String value = matcher.group(2);
	 
			System.out.printf("%s=[%s]%n", name, value);
		}
		
		
SearchCriteria criteria = new SearchCriteria();
		
		List<String> motcles = new ArrayList<String>();
		// Requete initiale sur un mot clef existant forcément
		motcles.add("bvet");
		
		SearchRequest request = new SearchRequest();
		criteria.setKeywords(motcles);
		criteria.setKeywordsLogicalLink(SearchCriteria.AND);
		request.setCriteria(criteria);
		
		Boolean isLoaded = request.calculateHits();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		Assert.assertTrue("Le nombre de résultat doit être calculé", request.getHits()>=0);
		
		
		
		isLoaded = request.fetchSummaries();
		
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		List<Summary> summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertFalse("La liste ne doit pas être vide", summaries.isEmpty());
		Assert.assertTrue("La liste doit contenir 10 élément", summaries.size()==10);
		
		
		request.setPageSize(20);
		request.setPagePosition(10);
		
		isLoaded = request.fetchSummaries();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertFalse("La liste ne doit pas être vide", summaries.isEmpty());
		Assert.assertTrue("La liste doit contenir 10 élément", summaries.size()==20);
		
	}

}
