package fr.sedoo.rbv.geonetwork.request;


import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkGroup;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfig.xml"})
public class TestUserGroupRequests 
{
	
	
	@Test
	public void testExist() throws Exception 
	{
		MetadataExistRequest request = new MetadataExistRequest("xx7d5ec60c-ca83-408e-a0b6-6871f4df06b0");
		Boolean result = request.execute();
		Assert.assertFalse("La fiche ne doit pas être présente", result);
		request = new MetadataExistRequest("7d5ec60c-ca83-408e-a0b6-6871f4df06b0");
		result = request.execute();
		Assert.assertTrue("La fiche doit pas être présente", result);
		
	}
	
	
	@Test
	public void testUserList() throws Exception 
	{
		UserRequest request = new UserRequest();
		List<GeonetworkUser> users = request.execute();
		Assert.assertTrue("La liste ne doit pas être vide", users.size()>0);
	}
	
	@Test
	public void testGroupList() throws Exception 
	{
		GroupRequest request = new GroupRequest();
		List<GeonetworkGroup> groups = request.execute();
		Assert.assertTrue("La liste ne doit pas être vide", groups.size()>0);
	}
	
}
