package fr.sedoo.rbv.geonetwork.request;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/contextProvider.xml",
		"classpath:/META-INF/spring/geonetworkConfig.xml" })
public class TestDebugMetadataByIdRequest extends AbstractAuthenticatedTest {
	GeonetworkUser bvetUser;

	@Test
	public void testMarshalling() throws Exception {
		String uuid = "85639fe7-6287-3a57-84f8-5c6d9e699b6c";
		MetadataByIdRequest request = new MetadataByIdRequest(uuid,
				getUserByName(TEST_USER_NAME));
		Boolean isLoaded = request.execute();
		Assert.assertTrue("La fiche doit être chargee", isLoaded);
		SedooMetadata metadata = request.getMetadata();
		Assert.assertEquals(metadata.getUuid(), uuid);

	}

}
