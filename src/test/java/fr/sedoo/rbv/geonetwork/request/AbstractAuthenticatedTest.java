package fr.sedoo.rbv.geonetwork.request;

import java.util.HashMap;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

public abstract class AbstractAuthenticatedTest {
	
	private static HashMap<String, GeonetworkUser> userMap;
	public final static String TEST_USER_NAME = "test";
	public final static String ADMIN_USER_NAME = "admin";
	public final static String FALSE_USER_NAME = "admins";
	
	protected static GeonetworkUser getUserByName(String name)
	{
		if (userMap == null)
		{
			init();
		}
		return userMap.get(name);
	}

	private static void init() 
	{
		userMap = new HashMap<String, GeonetworkUser>();

		GeonetworkUser bvetUser = new GeonetworkUser();
		bvetUser.setLogin(TEST_USER_NAME);
		bvetUser.setPassword("sedootest");
		
		GeonetworkUser adminUser = new GeonetworkUser();
		adminUser.setLogin(ADMIN_USER_NAME);
		adminUser.setPassword("admin");
		
		GeonetworkUser falseUser = new GeonetworkUser();
		falseUser.setLogin(FALSE_USER_NAME);
		falseUser.setPassword("adminx");
		
		userMap.put(TEST_USER_NAME, bvetUser);
		userMap.put(ADMIN_USER_NAME, adminUser);
		userMap.put(FALSE_USER_NAME, falseUser);
	}
	
	

}
