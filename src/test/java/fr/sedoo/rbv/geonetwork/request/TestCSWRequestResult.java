package fr.sedoo.rbv.geonetwork.request;


import junit.framework.Assert;

import org.junit.Test;

import fr.sedoo.commons.test.TestTools;


public class TestCSWRequestResult 
{
	
	
	@Test
	public void testAddMetadata() throws Exception 
	{
		String content = TestTools.getResourceFileContent(this.getClass(), "CSWRequestResultAdd.xml");
		CSWRequestResult result = CSWRequestResult.fromXML(content);
		Assert.assertEquals("Une fiche doit être indiquée comme étant ajoutée", 1, (int) result.getTotalInserted());
		Assert.assertEquals("Aucune fiche ne doit être indiquée comme étant modifiée", 0, (int) result.getTotalUpdated());
		Assert.assertEquals("Aucune fiche ne doit être indiquée comme étant supprimée", 0, (int) result.getTotalDeleted());
		String targetId="99999999-ca83-408e-a0b6-6871f4df06b0";
		Assert.assertEquals("L'identifiant doit être "+targetId, targetId, result.getIdentifier());
		
	}


}
