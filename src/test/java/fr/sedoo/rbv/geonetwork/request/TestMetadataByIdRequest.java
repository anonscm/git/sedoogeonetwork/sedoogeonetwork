package fr.sedoo.rbv.geonetwork.request;


import java.io.InputStream;

import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfig.xml"})
public class TestMetadataByIdRequest extends AbstractAuthenticatedTest 
{
	GeonetworkUser bvetUser;
	
	
	@Test
	public void testMarshalling() throws Exception 
	{
		//On supprime les fiches
		MyMetadataDeleteAllRequest myMetadataDeleteAllRequest = new MyMetadataDeleteAllRequest(getUserByName(TEST_USER_NAME));
		boolean isDeleted= myMetadataDeleteAllRequest.execute();
		Assert.assertTrue("La suppression doit être effectuée", isDeleted);
		
		InputStream resourceAsStream = this.getClass().getResourceAsStream("bvet.xml");
		String content = IOUtils.toString(resourceAsStream, "UTF-8");
		
		// On vérifie que la fiche a un identifiant
		MetadataSummary summary = MetadataSummary.fromXML(content);
		Assert.assertNotNull(summary.getUuid());
		
		// On ajoute la fois la fiche 
		MetadataAddOrUpdateWithIdRequest updateRequest = new MetadataAddOrUpdateWithIdRequest(content, getUserByName(TEST_USER_NAME));
		boolean isAdded = updateRequest.execute();
		Assert.assertTrue("La fiche doit être ajoutée", isAdded);
		
		MetadataByIdRequest request = new MetadataByIdRequest(summary.getUuid(), getUserByName(TEST_USER_NAME));
		Boolean isLoaded = request.execute();
		Assert.assertTrue("La fiche doit être chargee", isLoaded);
		SedooMetadata metadata = request.getMetadata();
		
	}

}
