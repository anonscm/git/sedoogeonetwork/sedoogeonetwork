package fr.sedoo.rbv.geonetwork.request;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestLoginRequest.class,
        TestMetadataAddWithoutIdRequest.class,
        TestMyMetadataDeleteAllRequest.class,
        TestCSWRequestResult.class,
        TestExistRequest.class,
        TestDeleteRequest.class,
        TestMetadataAddOrUpdateWithIdRequest.class
})
public class RequestSuite {

}
