package fr.sedoo.rbv.geonetwork.request;


import java.io.InputStream;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfig.xml"})
public class TestMyMetadataDeleteAllRequest extends AbstractAuthenticatedTest 
{
	GeonetworkUser bvetUser;
	
	
	@Test
	public void testBasicDeletion() throws Exception 
	{
	
		InputStream resourceAsStream = this.getClass().getResourceAsStream("bvet-idless.xml");
		String content = IOUtils.toString(resourceAsStream, "UTF-8");
		
		MetadataSummary summary = MetadataSummary.fromXML(content);
		
		Assert.assertNull(summary.getUuid());
		
		// On ajoute deux fiches sans identifiant
		MetadataAddWithoutIdRequest addRequest = new MetadataAddWithoutIdRequest(content, getUserByName(TEST_USER_NAME));
		boolean isAdded = addRequest.execute();
		Assert.assertTrue("La fiche doit être ajoutée", isAdded);
		isAdded = addRequest.execute();
		Assert.assertTrue("La fiche doit être ajoutée", isAdded);
		
		MySummaryRequest mySummaryRequest = new MySummaryRequest(getUserByName(TEST_USER_NAME));
		boolean result = mySummaryRequest.execute();
		Assert.assertTrue("Le listage doit avoir été effectué correctement", result);
		List<Summary> summaries = mySummaryRequest.getSummaries();
		Assert.assertTrue("La liste doit contenir plus de deux éléments", summaries.size()>=2);
		
		MyMetadataDeleteAllRequest request = new MyMetadataDeleteAllRequest(getUserByName(TEST_USER_NAME));
		boolean isDeleted = request.execute();
		Assert.assertTrue("La suppression doit avoir été effectuée correctement", isDeleted);
		mySummaryRequest.execute();
		summaries = mySummaryRequest.getSummaries();
		Assert.assertTrue("La liste doit être vide", summaries.size()==0);
		
	}

}
