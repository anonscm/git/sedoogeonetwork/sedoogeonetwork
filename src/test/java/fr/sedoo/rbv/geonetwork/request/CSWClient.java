package fr.sedoo.rbv.geonetwork.request;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.rbv.geonetwork.config.GeonetworkConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/META-INF/spring/contextProvider.xml", "classpath:/META-INF/spring/geonetworkConfig.xml" })
public class CSWClient
{

	@Test
	public void simpleTest() throws Exception
	{
		GeonetworkConfig config = new GeonetworkConfig()
		{

			@Override
			public String getCSWPublicationURL()
			{
				return "http://bd.amma-catch.org:8082/amma-catchWS2/WS/csw/default";
			}
		};
		SearchRequest request = new SearchRequest(config);
		SearchCriteria criteria = new SearchCriteria();
		List<String> keywords = new ArrayList<String>();
		keywords.add("africa");
		criteria.setKeywords(keywords);
		request.setCriteria(criteria);
		boolean execute = request.fetchSummaries();
		List<Summary> summaries = request.getSummaries();
		System.out.println(summaries);
	}

}
