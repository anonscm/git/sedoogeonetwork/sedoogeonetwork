package fr.sedoo.rbv.geonetwork.request;


import java.io.InputStream;

import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfig.xml"})
public class TmpGeonetworkRequests 
{
	GeonetworkUser bvetUser;
	
	
	@Test
	public void testMetadataLifeCycle() throws Exception 
	{
	
		InputStream resourceAsStream = this.getClass().getResourceAsStream("bvet.xml");
		String content = IOUtils.toString(resourceAsStream, "UTF-8");
		
		MetadataSummary summary = MetadataSummary.fromXML(content);
		
		Assert.assertNotNull(summary.getUuid());
		
		MetadataExistRequest existRequest = new MetadataExistRequest(summary.getUuid());
		Boolean exists = existRequest.execute();
		
		if (exists == true)
		{
			MetadataDeleteRequest deleteRequest = new MetadataDeleteRequest(summary.getUuid());
			boolean isDeleted = deleteRequest.execute();
			Assert.assertTrue("La fiche doit avoir été supprimée", isDeleted);
			exists = existRequest.execute();
			Assert.assertFalse("La fiche doit être abstente", exists);
			
		}
		
		MetadataAddWithoutIdRequest addRequest = new MetadataAddWithoutIdRequest(content, getBvetUser());
		boolean result = addRequest.execute();
		Assert.assertTrue("La fiche doit avoir été insérée", result); 
		
		exists = existRequest.execute();
		
		Assert.assertTrue("La fiche doit être présente", result);
		MetadataDeleteRequest deleteRequest = new MetadataDeleteRequest(summary.getUuid());
		boolean isDeleted = deleteRequest.execute();
		Assert.assertTrue("La fiche doit avoir été supprimée", isDeleted);
		exists = existRequest.execute();
		Assert.assertFalse("La fiche doit être abstente", exists);
	}
	
	private GeonetworkUser getBvetUser()
	{
		if (bvetUser == null)
		{
			bvetUser = new GeonetworkUser();
			bvetUser.setLogin("bvet");
			bvetUser.setPassword("sedoobvet");
		}
		return bvetUser;
	}
	


}
