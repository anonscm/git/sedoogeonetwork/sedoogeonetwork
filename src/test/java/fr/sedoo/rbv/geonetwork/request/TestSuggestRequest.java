package fr.sedoo.rbv.geonetwork.request;


import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfigLocal.xml"})
public class TestSuggestRequest extends AbstractAuthenticatedTest 
{
	
	@Test
	public void testSimple() throws Exception 
	{
		SuggestRequest request = new SuggestRequest();
		request.setFieldName("observatoryName");
		Boolean isLoaded = request.execute();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		Assert.assertNotNull("La liste des suggestions ne doit pas être nulle", request.getSuggestions());
		Assert.assertNotNull("La liste des suggestions ne doit pas être vide", request.getSuggestions().size()>0);
	}
}
