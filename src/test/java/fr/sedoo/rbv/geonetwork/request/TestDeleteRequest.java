package fr.sedoo.rbv.geonetwork.request;


import java.io.InputStream;

import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfig.xml"})
public class TestDeleteRequest extends AbstractAuthenticatedTest
{
	
	@Test
	public void testDelete() throws Exception 
	{
		InputStream resourceAsStream = this.getClass().getResourceAsStream("bvet-idless.xml");
		String content = IOUtils.toString(resourceAsStream, "UTF-8");
		MetadataAddWithoutIdRequest addRequest = new MetadataAddWithoutIdRequest(content, getUserByName(TEST_USER_NAME));
		boolean isAdded = addRequest.execute();
		Assert.assertTrue("La fiche doit être ajoutée", isAdded);
		String uuid = addRequest.getUuid();
		MetadataExistRequest request = new MetadataExistRequest(uuid, getUserByName(TEST_USER_NAME));
		Boolean exists = request.execute();
		Assert.assertTrue("La fiche doit être présente", exists);
		
		MetadataDeleteRequest deleteRequest = new MetadataDeleteRequest(uuid, getUserByName(TEST_USER_NAME));
		boolean isDeleted = deleteRequest.execute();
		Assert.assertTrue("La fiche doit être supprimée", isDeleted);
		
		request = new MetadataExistRequest(uuid, getUserByName(TEST_USER_NAME));
		exists = request.execute();
		Assert.assertFalse("La fiche ne doit pas être présente", exists);
	}
	
}
