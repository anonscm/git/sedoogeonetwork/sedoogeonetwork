package fr.sedoo.rbv.geonetwork.request;


import java.io.InputStream;

import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.commons.metadata.utils.domain.SedooMetadata;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfig.xml"})
public class TestMetadataAddOrUpdateWithIdRequest extends AbstractAuthenticatedTest 
{
	GeonetworkUser bvetUser;
	
	
	@Test
	public void testBasicUpdate() throws Exception 
	{
		//On supprime les fiches
		MyMetadataDeleteAllRequest myMetadataDeleteAllRequest = new MyMetadataDeleteAllRequest(getUserByName(TEST_USER_NAME));
		boolean isDeleted= myMetadataDeleteAllRequest.execute();
		Assert.assertTrue("La suppression doit être effectuée", isDeleted);
		
		InputStream resourceAsStream = this.getClass().getResourceAsStream("bvet.xml");
		String content = IOUtils.toString(resourceAsStream, "UTF-8");
		
		// On vérifie que la fiche a un identifiant
		MetadataSummary summary = MetadataSummary.fromXML(content);
		Assert.assertNotNull(summary.getUuid());
		
		// On ajoute une premiere fois la fiche 
		MetadataAddOrUpdateWithIdRequest updateRequest = new MetadataAddOrUpdateWithIdRequest(content, getUserByName(TEST_USER_NAME));
		boolean isAdded = updateRequest.execute();
		Assert.assertTrue("La fiche doit être ajoutée", isAdded);
		
		MetadataByIdRequest request = new MetadataByIdRequest(summary.getUuid(), getUserByName(TEST_USER_NAME));
		Boolean isLoaded = request.execute();
		Assert.assertTrue("La fiche doit être chargee", isLoaded);
		SedooMetadata metadata = request.getMetadata();
		//Assert.assertEquals("Les titres doivent correspondre", summary.getResourceTitle(), metadata.getResourceTitle(null)); 
		
		// On ajoute la version modifiée de la fiche
		resourceAsStream = this.getClass().getResourceAsStream("updatedBvet.xml");
		String updatedContent = IOUtils.toString(resourceAsStream, "UTF-8");
		MetadataSummary updatedSummary = MetadataSummary.fromXML(updatedContent);
		Assert.assertEquals("Les uuid doivent correspondre", summary.getUuid(), updatedSummary.getUuid()); 
		
		updateRequest = new MetadataAddOrUpdateWithIdRequest(updatedContent, getUserByName(TEST_USER_NAME));
		isAdded = updateRequest.execute();
		
		MetadataExistRequest existRequest = new MetadataExistRequest(summary.getUuid(),getUserByName(TEST_USER_NAME));
		Assert.assertTrue("La fiche doit être présente", existRequest.execute());
		
		isLoaded = request.execute();
		Assert.assertTrue("La fiche doit être chargee", isLoaded);
		SedooMetadata updatedMetadata = request.getMetadata();
		//Assert.assertEquals("Les titres doivent correspondre", updatedSummary.getResourceTitle(), updatedMetadata.getResourceTitle(null));
		
		//Ajout d'un pause nécessaire pour Géonetwork ?...
		Thread.sleep(1000);
		MetadataDeleteRequest metadataDeleteRequest = new MetadataDeleteRequest(summary.getUuid(), getUserByName(TEST_USER_NAME));
		metadataDeleteRequest.execute();
		
	}
	
	@Test(expected = Exception.class)
	public void testAddWithoutUuid() throws Exception 
	{
		InputStream resourceAsStream = this.getClass().getResourceAsStream("bvet-idless.xml");
		String content = IOUtils.toString(resourceAsStream, "UTF-8");
		
		// On vérifie que la fiche n'a pas d'identifiant
		MetadataSummary summary = MetadataSummary.fromXML(content);
		Assert.assertNull(summary.getUuid());
		
		MetadataAddOrUpdateWithIdRequest updateRequest = new MetadataAddOrUpdateWithIdRequest(content, getUserByName(TEST_USER_NAME));
		updateRequest.execute();
		
		
	}

}
