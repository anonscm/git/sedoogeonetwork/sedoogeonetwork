package fr.sedoo.rbv.geonetwork.request;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkConfig.xml"})
public class TestSummaryByIdRequest extends AbstractAuthenticatedTest 
{
	GeonetworkUser bvetUser;
	
	
	@Test
	public void testRecuperation() throws Exception 
	{
		List<String> uuids = new ArrayList<String>();
//		uuids.add("6bf678bb-8fc2-3c4a-b964-1750b7d24c4c");
//		uuids.add("4c16dff7-f6c3-3658-85dc-cd0d948ad261");
//		uuids.add("d3a3e8e4-70eb-3767-bab0-8f01f0bb4c54");
//		uuids.add("59a18e97-8b0d-31ca-9126-288183f8f696");
		uuids.add("5DCC4C12-07BC-4A1D-B9C4-57061932AC6C");
		
		
		SummaryByIdRequest request = new SummaryByIdRequest();
		request.setIds(uuids);
		
		Boolean isLoaded = request.execute();
		Assert.assertTrue("La requête doit être exécutée", isLoaded);
		List<Summary> summaries = request.getSummaries();
		
		Assert.assertNotNull("La liste ne doit pas être nulle", summaries);
		Assert.assertFalse("La liste ne doit pas être vide", summaries.isEmpty());
		Assert.assertTrue("La liste doit contenir autant d'élément que d'id", uuids.size()==summaries.size());
		String idDump1="";
		String idDump2="";
		Iterator<String> iterator = uuids.iterator();
		while (iterator.hasNext()) {
			String current = iterator.next();
			idDump1 = idDump1+current+"|";
		}
		
		Iterator<Summary> iterator2 = summaries.iterator();
		while (iterator2.hasNext()) {
			Summary current = iterator2.next();
			idDump2 = idDump2+current.getUuid()+"|";
		}
		Assert.assertEquals("Les listes doivent être dans le même ordre", idDump2, idDump1);
		
	}

}
