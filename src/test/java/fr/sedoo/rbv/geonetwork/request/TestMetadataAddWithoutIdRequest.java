package fr.sedoo.rbv.geonetwork.request;


import java.io.InputStream;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/geonetworkMonGeosourceConfig.xml"})
public class TestMetadataAddWithoutIdRequest extends AbstractAuthenticatedTest 
{
	GeonetworkUser bvetUser;
	
	
	@Test
	public void testBasicAdd() throws Exception 
	{
	
//		MySummaryRequest mySummaryRequest = new MySummaryRequest(getUserByName(TEST_USER_NAME));
		MySummaryRequest mySummaryRequest = new MySummaryRequest();
		boolean result = mySummaryRequest.execute();
		Assert.assertTrue("Le listage doit avoir été effectué correctement", result);
		List<Summary> summaries = mySummaryRequest.getSummaries();
		int initialSize = summaries.size();
		
		InputStream resourceAsStream = this.getClass().getResourceAsStream("bvet-idless.xml");
		String content = IOUtils.toString(resourceAsStream, "UTF-8");
		
		// On vérifie que la fiche n'a pas d'identifiant
		MetadataSummary summary = MetadataSummary.fromXML(content);
		
		Assert.assertNull(summary.getUuid());
		
		// On ajoute deux fiches sans identifiant
		MetadataAddWithoutIdRequest addRequest = new MetadataAddWithoutIdRequest(content, getUserByName(TEST_USER_NAME));
		boolean isAdded = addRequest.execute();
		Assert.assertTrue("La fiche doit être ajoutée", isAdded);
		String id1 =  addRequest.getUuid();
		isAdded = addRequest.execute();
		Assert.assertTrue("La fiche doit être ajoutée", isAdded);
		String id2 =  addRequest.getUuid();
		
		mySummaryRequest = new MySummaryRequest(getUserByName(TEST_USER_NAME));
		result = mySummaryRequest.execute();
		Assert.assertTrue("Le listage doit avoir été effectué correctement", result);
		summaries = mySummaryRequest.getSummaries();
		Assert.assertEquals("La liste doit contenir deux éléments de plus", initialSize+2,summaries.size());
		
		MetadataExistRequest existRequest = new MetadataExistRequest(id1,getUserByName(TEST_USER_NAME));
		Assert.assertTrue("La fiche doit être présente", existRequest.execute());
		
		existRequest = new MetadataExistRequest(id2,getUserByName(TEST_USER_NAME));
		Assert.assertTrue("La fiche doit être présente", existRequest.execute());
		
		MetadataDeleteRequest metadataDeleteRequest = new MetadataDeleteRequest(id1, getUserByName(TEST_USER_NAME));
		metadataDeleteRequest.execute();
		metadataDeleteRequest = new MetadataDeleteRequest(id2, getUserByName(TEST_USER_NAME));
		metadataDeleteRequest.execute();
		
	}
	
	@Test(expected = Exception.class)
	public void testAddWithUuid() throws Exception 
	{
		InputStream resourceAsStream = this.getClass().getResourceAsStream("bvet.xml");
		String content = IOUtils.toString(resourceAsStream, "UTF-8");
		
		// On vérifie que la fiche a un identifiant
		MetadataSummary summary = MetadataSummary.fromXML(content);
		Assert.assertNotNull(summary.getUuid());
		
		MetadataAddWithoutIdRequest addRequest = new MetadataAddWithoutIdRequest(content, getUserByName(TEST_USER_NAME));
		addRequest.execute();
		
		
	}

}
