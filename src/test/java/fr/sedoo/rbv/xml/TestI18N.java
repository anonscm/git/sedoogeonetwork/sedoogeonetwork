package fr.sedoo.rbv.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.binder.DigesterLoader;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.xml.sax.InputSource;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.rbv.geonetwork.request.SummaryByIdRequest;
import fr.sedoo.rbv.geonetwork.request.csw.SummaryModule;

public class TestI18N {
	
	@Test
	public void testDeserialization() throws Exception
	{
		String xml = FileUtils.readFileToString(new File("C:/Users/francois/Developpement/workspace-rbvmetadata/geonetwork-utils/src/test/resources/summary.xml"));
		DigesterLoader digesterLoader = DigesterLoader.newLoader(new SummaryModule());
		Digester digester = digesterLoader.newDigester();
		digester.setValidating(false);
		ArrayList<String> languages = new ArrayList<String>();
		languages.add("eng");
		languages.add("fre");
		xml = SummaryByIdRequest.adaptTranslationTag(xml, languages);
		ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes());
		Reader reader = new InputStreamReader(inputStream,"UTF-8");
		InputSource is = new InputSource(reader);
		is.setEncoding("UTF-8");

		List<Summary> tmp = digester.parse(is);
		System.out.println(tmp);
	}

	
	
	
}
