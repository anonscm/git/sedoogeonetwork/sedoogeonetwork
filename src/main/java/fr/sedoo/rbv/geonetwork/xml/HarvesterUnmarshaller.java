package fr.sedoo.rbv.geonetwork.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.digester3.Digester;
import org.xml.sax.InputSource;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkHarvester;

public class HarvesterUnmarshaller 
{
	public static List<GeonetworkHarvester> fromXML(String xml) throws Exception
	{
		ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes());
		Reader reader = new InputStreamReader(inputStream,"UTF-8");
		InputSource is = new InputSource(reader);
		is.setEncoding("UTF-8");
		Digester digester = new Digester();
        digester.setValidating(false);
        digester.addObjectCreate("nodes",ArrayList.class);
        digester.addObjectCreate("nodes/node", GeonetworkHarvester.class);
        digester.addSetProperties("nodes/node","id","id");
        digester.addBeanPropertySetter("nodes/node/site/name","name");
        digester.addBeanPropertySetter("nodes/node/site/uuid","uuid");
        digester.addSetNext("nodes/node","add");
       	ArrayList<GeonetworkHarvester> result = digester.parse(is);
		return result;
        
	}
	
}
