package fr.sedoo.rbv.geonetwork.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.digester3.Digester;
import org.xml.sax.InputSource;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkMetadataInfo;

public class GeonetworkMetadataInfoUnmarshaller 
{
	public static List<GeonetworkMetadataInfo> fromXML(String xml) throws Exception
	{
		ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes());
		Reader reader = new InputStreamReader(inputStream,"UTF-8");
		InputSource is = new InputSource(reader);
		is.setEncoding("UTF-8");
		Digester digester = new Digester();
        digester.setValidating(false);
        digester.addObjectCreate("response",ArrayList.class);
        digester.addObjectCreate("response/metadata", GeonetworkMetadataInfo.class);
        digester.addBeanPropertySetter("response/metadata/geonet:info/uuid","metadataUuid");
        digester.addBeanPropertySetter("response/metadata/geonet:info/source","harvesterUuid");
        digester.addSetNext("response/metadata","add");
       	ArrayList<GeonetworkMetadataInfo> result = digester.parse(is);
		return result;
        
	}
	
}
