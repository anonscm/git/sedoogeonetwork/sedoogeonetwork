package fr.sedoo.rbv.geonetwork.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.apache.commons.digester3.Digester;
import org.xml.sax.InputSource;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkGroup;

public class GroupUnmarshaller 
{
	public static List<GeonetworkGroup> fromXML(String xml) throws Exception
	{
		ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes());
		Reader reader = new InputStreamReader(inputStream,"UTF-8");
		InputSource is = new InputSource(reader);
		is.setEncoding("UTF-8");
		Digester digester = new Digester();
        digester.setValidating(false);
        digester.addObjectCreate("response",GroupList.class);
        digester.addObjectCreate("response/record",GeonetworkGroup.class);
        digester.addBeanPropertySetter("response/record/name","name");
        digester.addBeanPropertySetter("response/record/id","id");
        digester.addBeanPropertySetter("response/record/description","observation");
        digester.addSetNext("response/record","add");
       	GroupList result = digester.parse(is);
		return result;
        
	}
	
}
