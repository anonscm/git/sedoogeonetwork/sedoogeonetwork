package fr.sedoo.rbv.geonetwork.xml;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.apache.commons.digester3.Digester;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

public class GeonetworkUserUnmarshaller 
{
	public static List<GeonetworkUser> fromXML(String xml)
	{
		
		Digester digester = new Digester();
        digester.setValidating(false);
        digester.addObjectCreate("response",GeonetworkUserList.class);
        digester.addObjectCreate("response/record",GeonetworkUser.class);
        digester.addBeanPropertySetter("response/record/username","login");
        digester.addBeanPropertySetter("response/record/surname","firstName");
        digester.addBeanPropertySetter("response/record/name","lastName");
        digester.addBeanPropertySetter("response/record/email","email");
        digester.addCallMethod("response/record/profile","setAdministrator",1);
        digester.addCallParam("response/record/profile",0);
        digester.addSetNext("response/record","add");
        try {
        	GeonetworkUserList result = digester.parse(new ByteArrayInputStream(xml.getBytes()));
			return result;
		} catch (Exception e) 
		{
			return new GeonetworkUserList();// 
		}
	}
	
}
