package fr.sedoo.rbv.geonetwork.request.csw;

import java.util.ArrayList;

import org.apache.commons.digester3.binder.AbstractRulesModule;
import org.geotoolkit.metadata.iso.extent.DefaultGeographicBoundingBox;

import fr.sedoo.rbv.geonetwork.request.GeoSummary;

public class GeoSummaryModule extends AbstractRulesModule implements DCConstants, CSWConstants
{

	@Override
	public void configure()
	{
		forPattern(CSW_NAMESPACE + ":GetRecordsResponse").createObject().ofType(ArrayList.class).then().setProperties();
		forPattern("*/csw:SearchResults/gmd:MD_Metadata").createObject().ofType(GeoSummary.class).then().setProperties().then().setNext("add");
		forPattern("*/gmd:fileIdentifier/gco:CharacterString").setBeanProperty().withName("uuid");
		forPattern("*/gmd:abstract/gco:CharacterString").setBeanProperty().withName("resourceAbstract");
		forPattern("*/gmd:abstract/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString").callMethod("addAbstractTranslation").withParamTypes("java.lang.String","java.lang.String");
		forPattern("*/gmd:abstract/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString/lang").callParam().ofIndex(0);
		forPattern("*/gmd:abstract/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString/value").callParam().ofIndex(1);	
		forPattern("*/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString").setBeanProperty().withName("resourceTitle");
		forPattern("*/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString").callMethod("addTitleTranslation").withParamTypes("java.lang.String","java.lang.String");
		forPattern("*/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString/"+SummaryModule.LANG_TAG).callParam().ofIndex(0);
		forPattern("*/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString/"+SummaryModule.VALUE_TAG).callParam().ofIndex(1);	
		forPattern("*/gmd:EX_GeographicBoundingBox").createObject().ofType(DefaultGeographicBoundingBox.class).then().setProperties().then().setNext("setBox");
		forPattern(
				"*/gmd:MD_Metadata/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:westBoundLongitude/gco:Decimal")
				.setBeanProperty().withName("westBoundLongitude");
		forPattern(
				"*/gmd:MD_Metadata/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:eastBoundLongitude/gco:Decimal")
				.setBeanProperty().withName("eastBoundLongitude");
		forPattern(
				"*/gmd:MD_Metadata/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:northBoundLatitude/gco:Decimal")
				.setBeanProperty().withName("northBoundLatitude");
		forPattern(
				"*/gmd:MD_Metadata/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox/gmd:southBoundLatitude/gco:Decimal")
				.setBeanProperty().withName("southBoundLatitude");
		
	}
}
