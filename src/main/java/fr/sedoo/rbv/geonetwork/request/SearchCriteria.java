package fr.sedoo.rbv.geonetwork.request;

import java.util.ArrayList;
import java.util.List;

import org.opengis.metadata.extent.GeographicBoundingBox;

public class SearchCriteria 
{
	public static final String AND = "And";
	public static final String OR = "Or";
	
	private GeographicBoundingBox boundingBox;
	private List<String> keywords = new ArrayList<String>();
	private String keywordsLogicalLink=AND;
	private String startDate;
	private String endDate;
	private List<String> observatories = new ArrayList<String>();
	private String parentUuid="";
	
	private boolean includesDatasets = false;
	private boolean includesExperimentalSites = false;
	private boolean includesObservatories = false;

	public GeographicBoundingBox getBoundingBox() {
		return boundingBox;
	}

	public void setBoundingBox(GeographicBoundingBox boundingBox) {
		this.boundingBox = boundingBox;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) 
	{
		if (keywords != null)
		{
			this.keywords = keywords;
		}
	}

	public String getKeywordsLogicalLink() {
		return keywordsLogicalLink;
	}

	public void setKeywordsLogicalLink(String keywordsLogicalLink) {
		this.keywordsLogicalLink = keywordsLogicalLink;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public List<String> getObservatories() {
		return observatories;
	}

	public void setObservatories(List<String> observatories) {
		this.observatories = observatories;
	}
	
	public void setIncludesObservatories(boolean value) {
		includesObservatories = value;
	}
	
	public void setIncludesDatasets(boolean value) {
		includesDatasets = value;
	}
	
	public void setIncludesExperimentalSites(boolean value) {
		includesExperimentalSites = value;
	}

	public boolean getIncludesObservatories() {
		return includesObservatories;
	}
	
	public boolean getIncludesDatasets() {
		return includesDatasets;
	}
	
	public boolean getIncludesExperimentalSites() {
		return includesExperimentalSites;
	}

	public String getParentUuid() {
		return parentUuid;
	}

	public void setParentUuid(String parentUuid) {
		this.parentUuid = parentUuid;
	}
		
}
