package fr.sedoo.rbv.geonetwork.request;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

public class MakePublicRequest extends AuthenticatedRequest
{
	private Long metadataId;
	private Long groupId;

	public MakePublicRequest(GeonetworkUser user, Long metadataId, Long groupId) 
	{
		super(user);
		this.metadataId = metadataId;
		this.groupId = groupId;
	}
	
	public boolean execute() throws Exception
	{
//		boolean connectionResult = login();
//		if (connectionResult == false)
//		{
//			throw new Exception("Invalid login");
//		}
//		else
//		{
//			String privilegeURL = getConfig().getPrivilegeURL();
//			String url = privilegeURL+"?id="+metadataId+"&_"+groupId+"_0";
//			GetMethod get = new GetMethod(url);
//			int result = httpclient.executeMethod(get);
//			if (result == HttpStatus.SC_OK)
//			{
//				return true;
//			}
//		}
		return false;
	}

}
