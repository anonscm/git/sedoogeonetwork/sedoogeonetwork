package fr.sedoo.rbv.geonetwork.request;

import org.apache.commons.httpclient.HttpClient;

import fr.sedoo.commons.spring.SpringBeanFactory;
import fr.sedoo.rbv.geonetwork.config.GeonetworkConfig;

public abstract class AbstractGeonetworkClient 
{
	protected HttpClient httpclient= new HttpClient();
	
	private static  GeonetworkConfig globalConfig;
	private GeonetworkConfig localConfig;
	
	public AbstractGeonetworkClient(GeonetworkConfig config)
	{
		setLocalConfig(config);
	}
	
	public AbstractGeonetworkClient()
	{
	}
	
	public static void initGlobalConfig()
	{
		SpringBeanFactory factory = new SpringBeanFactory();
		globalConfig = (GeonetworkConfig) factory.getBeanByName("geoNetworkConfigInstance");	
	}
	
	public GeonetworkConfig getConfig()
	{
		if (getLocalConfig() != null)
		{
			return getLocalConfig();
		}
		else
		{
			if (globalConfig == null)
			{
				initGlobalConfig();
			}	
			return globalConfig;
		}
	}

	public GeonetworkConfig getLocalConfig() {
		return localConfig;
	}

	public void setLocalConfig(GeonetworkConfig localConfig) {
		this.localConfig = localConfig;
	}
	
}
