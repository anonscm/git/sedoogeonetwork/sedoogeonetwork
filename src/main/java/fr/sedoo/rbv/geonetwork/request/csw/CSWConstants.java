package fr.sedoo.rbv.geonetwork.request.csw;

public interface CSWConstants {

	static final String CSW_NAMESPACE="csw";
	static final String RECORD="Record";
	static final String GET_RECORD_BY_ID_RESPONSE="GetRecordByIdResponse";
	
}
