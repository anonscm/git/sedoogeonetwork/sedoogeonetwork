package fr.sedoo.rbv.geonetwork.request;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.jdom2.Document;
import org.jdom2.Element;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkHarvester;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.xml.HarvesterUnmarshaller;

public class HarvestRequest extends AuthenticatedRequest
{

	private String observatoryName;
	private boolean success = false;

	public HarvestRequest(GeonetworkUser user ) 
	{
		super(user);
	}

	public HarvestRequest() 
	{
		super();
	}

	public void execute() throws Exception
	{
		String id = getHarvesterIdFromObservatoryName();
		activateHarvester(id);
		runHarvester(id);
		desactivateHarvester(id);
		this.setSuccess(true);
	}

	private void desactivateHarvester(String id) throws Exception
	{
		launchAction(id, getConfig().getHarvesterDesactivateURL());
	}

	private void runHarvester(String id) throws Exception
	{
		launchAction(id, getConfig().getHarvesterRunURL());
	}

	private void activateHarvester(String id) throws Exception
	{
		launchAction(id, getConfig().getHarvesterActivateURL());
	}

	private void launchAction(String id, String url) throws Exception{

		Element request = new Element("request");
		Element idElement = new Element("id");
		idElement.setText(id);
		request.addContent(idElement);

		PostMethod post = new PostMethod(url);

		try {
			String postData = getString(new Document(request));
			post.setRequestEntity(new StringRequestEntity(postData,"application/xml", "UTF8"));

			String rawXml = executePostMethod(post);

			if ((rawXml.contains("ok")== false) && (rawXml.contains("already-active")==false))
			{
				throw new Exception();
			}
			return;
		}
		catch (Exception e)
		{	
			throw e;
		}
		finally {
			post.releaseConnection();
		}
	}


	public String getHarvesterIdFromObservatoryName() throws Exception
	{

		Element request = new Element("request");
		String userListURL = getConfig().getHarvesterListURL();
		PostMethod post = new PostMethod(userListURL);

		try {
			String postData = getString(new Document(request));
			post.setRequestEntity(new StringRequestEntity(postData,"application/xml", "UTF8"));
			String rawXml = executePostMethod(post);
			List<GeonetworkHarvester> list = HarvesterUnmarshaller.fromXML(rawXml);
			Iterator<GeonetworkHarvester> iterator = list.iterator();
			while (iterator.hasNext()) {
				GeonetworkHarvester geonetworkHarvester = (GeonetworkHarvester) iterator.next();
				if (geonetworkHarvester.getName().toLowerCase().contains(observatoryName.toLowerCase()))
				{
					return geonetworkHarvester.getId();
				}
			}
			throw new Exception("No harvester");
		}
		catch (Exception e)
		{	
			throw e;
		}
		finally {
			post.releaseConnection();
		}
	}


	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getObservatoryName() {
		return observatoryName;
	}

	public void setObservatoryName(String observatoryName) {
		this.observatoryName = observatoryName;
	}

}
