package fr.sedoo.rbv.geonetwork.request;

import org.opengis.metadata.extent.GeographicBoundingBox;

import fr.sedoo.commons.metadata.utils.domain.Summary;

public class GeoSummary extends Summary
{
	private GeographicBoundingBox box;

	public GeographicBoundingBox getBox()
	{
		return box;
	}

	public void setBox(GeographicBoundingBox box)
	{
		this.box = box;
	}

}
