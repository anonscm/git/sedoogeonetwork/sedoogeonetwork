package fr.sedoo.rbv.geonetwork.request;

import java.io.ByteArrayInputStream;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.binder.DigesterLoader;

public class MetadataSummary {

	private String uuid;
	private String resourceTitle; 

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	public static MetadataSummary fromXML(String rawXml) throws Exception
	{
		DigesterLoader digesterLoader = DigesterLoader.newLoader(new MetadataSummaryModule());
		Digester digester = digesterLoader.newDigester();
        digester.setValidating(false);
        MetadataSummary result = digester.parse(new ByteArrayInputStream(rawXml.getBytes()));
        return result;
	}

	public String getResourceTitle() {
		return resourceTitle;
	}

	public void setResourceTitle(String resourceTitle) {
		this.resourceTitle = resourceTitle;
	}
}
