package fr.sedoo.rbv.geonetwork.request;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.binder.DigesterLoader;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.opengis.metadata.extent.GeographicBoundingBox;
import org.xml.sax.InputSource;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.rbv.geonetwork.config.GeonetworkConfig;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.csw.DCSummaryModule;

public class SearchRequest extends AbstractCSWRequest
{

	public static final String SERIES = "series";
	public static final String DATASET = "dataset";
	private static final String OBSERVATORY_LEVEL_NAME="observatoryLevel";
	public static final String EXPERIMENTAL_SITE_LEVEL_NAME="experimentalSiteLevel";
	private List<Summary> summaries = new ArrayList<Summary>();
	private SearchCriteria criteria;
	private boolean loginNeeded = true;

	private int hits;
	private int pagePosition = DEFAULT_PAGE_POSITION;
	private int pageSize = DEFAULT_PAGE_SIZE;

	public final static int MAX_HIT_NUMBER = 1000;
	private final static int DEFAULT_PAGE_SIZE = 10;
	public final static int DEFAULT_PAGE_POSITION = 1;

	private static Pattern pattern = Pattern.compile("([a-zA-Z_0-9]+)=\"(.*?)\"");
	private ArrayList<String> displayLanguages;

	public SearchRequest(GeonetworkUser user)
	{
		super(user);
	}

	public SearchRequest(GeonetworkConfig config)
	{
		super(config);
	}

	public SearchRequest()
	{
		super();
	}

	public boolean calculateHits() throws Exception
	{
		hits = -1;

		if (getCriteria() == null)
		{
			throw new Exception("No criteria");
		}

			Element request = getHitElement();
			String cswURL = getConfig().getCSWPublicationURL();
			PostMethod post = new PostMethod(cswURL);

			try
			{
				String postData = getString(new Document(request));
				post.setRequestEntity(new StringRequestEntity(postData, "application/xml", UTF8));
					String rawXml = executePostMethod(post);
					if (rawXml.length() > 0)
					{
						Matcher matcher = pattern.matcher(rawXml);
						while (matcher.find())
						{
							String name = matcher.group(1);
							if (name.compareToIgnoreCase("numberOfRecordsMatched") == 0)
							{
								hits = new Integer(matcher.group(2));
								return true;
							}
						}

						return false;
					} else
					{
						throw new Exception();
					}
			} catch (Exception e)
			{
				throw new Exception(e);
			} finally
			{
				post.releaseConnection();
			}


	}

	public boolean fetchSummaries() throws Exception
	{
		summaries.clear();
		if (getCriteria() == null)
		{
			throw new Exception("No criteria");
		}

		

			Element request = getSummaryElement();
			String cswURL = getConfig().getCSWPublicationURL();
			PostMethod post = new PostMethod(cswURL);

			try
			{
				String postData = getString(new Document(request));
				post.setRequestEntity(new StringRequestEntity(postData, "application/xml", UTF8));
					String rawXml = executePostMethod(post);
					if (rawXml.length() > 0)
					{
						/*
						 * Dans une premère requête on ne récupère que les UUID
						 * concernés
						 */
						DigesterLoader digesterLoader = DigesterLoader.newLoader(new DCSummaryModule());
						Digester digester = digesterLoader.newDigester();
						digester.setValidating(false);

						ByteArrayInputStream inputStream = new ByteArrayInputStream(rawXml.getBytes());
						Reader reader = new InputStreamReader(inputStream,"UTF-8");
						InputSource is = new InputSource(reader);
						is.setEncoding("UTF-8");
						
						
						List<Summary> aux = digester.parse(is);

						SummaryByIdRequest byIdRequest = new SummaryByIdRequest();
						byIdRequest.setDisplayLanguages(displayLanguages);
						byIdRequest.setIds(toStringList(aux));
						boolean execution = byIdRequest.execute();
						if (execution == false)
						{
							return false;
						} else
						{
							summaries = byIdRequest.getSummaries();
						}

						return true;
					} else
					{
						throw new Exception();
					}
				
			} catch (Exception e)
			{
				throw new Exception(e);
			} finally
			{
				post.releaseConnection();
			}

	}

	private List<String> toStringList(List<Summary> source)
	{
		List<String> list = new ArrayList<String>();
		if (source != null)
		{
			Iterator<Summary> iterator = source.iterator();
			while (iterator.hasNext())
			{
				list.add(iterator.next().getUuid());

			}
		}
		return list;
	}

	private Element getHitElement()
	{
		return getSearchElement("hits", MAX_HIT_NUMBER, DEFAULT_PAGE_POSITION);
	}

	private Element getSummaryElement()
	{
		return getSearchElement("results", getPageSize(), getPagePosition());
	}

	private Element getSearchElement(String resultType, int pageSize, int maxRecords)
	{
		Element getRecordsElement = new Element("GetRecords", cswNamespace).setAttribute("service", "CSW").setAttribute("version", "2.0.2").setAttribute("resultType", resultType)
				.setAttribute("outputSchema", "csw:Record").setAttribute("maxRecords", "" + pageSize).setAttribute("startPosition", "" + pagePosition);

		Element queryElement = new Element("Query", cswNamespace).setAttribute("typesNames", "gmd:MD_Metadata");
		Element constraintElement = new Element("Constraint", cswNamespace).setAttribute("version", "1.1.0");
		Element filterElement = new Element("Filter", ogcNamespace);
		Element globalAndElement = new Element("And", ogcNamespace);

		getRecordsElement.addContent(queryElement);
		queryElement.addContent(constraintElement);
		constraintElement.addContent(filterElement);
		filterElement.addContent(globalAndElement);

		GeographicBoundingBox boundingBox = getCriteria().getBoundingBox();

		if ((boundingBox != null) && (isValid(boundingBox)))
		{
			Element withinElement = new Element("Within", ogcNamespace);
			Element propertyNameElement = new Element("PropertyName", ogcNamespace).setText("ows:BoundingBox");
			Element envelopeElement = new Element("Envelope", gmlNamespace);
			Element lowerCornerElement = new Element("lowerCorner", gmlNamespace).setText(boundingBox.getWestBoundLongitude() + " " + boundingBox.getSouthBoundLatitude());
			Element upperCornerElement = new Element("upperCorner", gmlNamespace).setText(boundingBox.getEastBoundLongitude() + " " + boundingBox.getNorthBoundLatitude());

			globalAndElement.addContent(withinElement);
			withinElement.addContent(propertyNameElement);
			withinElement.addContent(envelopeElement);
			envelopeElement.addContent(lowerCornerElement);
			envelopeElement.addContent(upperCornerElement);
		}

		List<String> keywords = getCriteria().getKeywords();

		if (keywords.isEmpty() == false)
		{
			Element keywordLogicalLinkElement = new Element(getCriteria().getKeywordsLogicalLink(), ogcNamespace);

			Iterator<String> iterator = keywords.iterator();
			while (iterator.hasNext())
			{
				String current = iterator.next();
				Element propertyIsLikeElement = new Element("PropertyIsLike", ogcNamespace).setAttribute("singleChar", " ").setAttribute("escape", "\\\\").setAttribute("wildCard", "%");
				Element propertyNameElement = new Element("PropertyName", ogcNamespace).setText("AnyText");
				Element literalElement = new Element("Literal", ogcNamespace).setText("%" + current + "%");
				propertyIsLikeElement.addContent(propertyNameElement);
				propertyIsLikeElement.addContent(literalElement);
				keywordLogicalLinkElement.addContent(propertyIsLikeElement);
			}
			globalAndElement.addContent(keywordLogicalLinkElement);
		}
		
		if (StringUtils.isNotEmpty(getCriteria().getParentUuid()))
		{
			Element propertyIsLikeElement = new Element("PropertyIsLike", ogcNamespace).setAttribute("singleChar", " ").setAttribute("escape", "\\\\").setAttribute("wildCard", "%");
			Element propertyNameElement = new Element("PropertyName", ogcNamespace).setText("ParentIdentifier");
			Element literalElement = new Element("Literal", ogcNamespace).setText("%" + getCriteria().getParentUuid() + "%");
			propertyIsLikeElement.addContent(propertyNameElement);
			propertyIsLikeElement.addContent(literalElement);
			
			globalAndElement.addContent(propertyIsLikeElement);
		}
		
		//Gestion des niveaux de fiches: Observatoire, Site exp�rimental et Dataset
		//Par d�faut, le HierachyLevel est mapp� par Geosource/Geonetwork sous la propri�t� "Type" ce qui permet facilement de diff�rentier
		//les Dataset des autres autres fiche.
		//Pour diff�rencier les observatoires des sites exp�rimentaux, il serait possible de modifier la configuration de geonetwork pour
		//ajouter hierarchyLevelName comme propri�t�s mapp�e. Au lieu de cela on va utiliser le fait que les valeurs observatoryLevel et experimentalSiteLevel sont assez
		//significative pour faire une requ�te de type AnyText.
		
		if (getCriteria().getIncludesDatasets() || getCriteria().getIncludesExperimentalSites() || getCriteria().getIncludesObservatories())
		{
			Element orElement = new Element(SearchCriteria.OR, ogcNamespace);

			if (getCriteria().getIncludesObservatories())
			{
				Element andElement = new Element(SearchCriteria.AND, ogcNamespace);
				
				Element propertyIsLikeElement = new Element("PropertyIsLike", ogcNamespace).setAttribute("singleChar", " ").setAttribute("escape", "\\\\").setAttribute("wildCard", "%");
				Element propertyNameElement = new Element("PropertyName", ogcNamespace).setText("Type");
				Element literalElement = new Element("Literal", ogcNamespace).setText(SERIES);
				propertyIsLikeElement.addContent(propertyNameElement);
				propertyIsLikeElement.addContent(literalElement);
				
				Element propertyIsLikeElement2 = new Element("PropertyIsLike", ogcNamespace).setAttribute("singleChar", " ").setAttribute("escape", "\\\\").setAttribute("wildCard", "%");
				Element propertyNameElement2 = new Element("PropertyName", ogcNamespace).setText("AnyText");
				Element literalElement2 = new Element("Literal", ogcNamespace).setText(OBSERVATORY_LEVEL_NAME);
				propertyIsLikeElement2.addContent(propertyNameElement2);
				propertyIsLikeElement2.addContent(literalElement2);
				
				andElement.addContent(propertyIsLikeElement);
				andElement.addContent(propertyIsLikeElement2);
				orElement.addContent(andElement);
			}
			
			if (getCriteria().getIncludesExperimentalSites())
			{
				Element andElement = new Element(SearchCriteria.AND, ogcNamespace);
				
				Element propertyIsLikeElement = new Element("PropertyIsLike", ogcNamespace).setAttribute("singleChar", " ").setAttribute("escape", "\\\\").setAttribute("wildCard", "%");
				Element propertyNameElement = new Element("PropertyName", ogcNamespace).setText("Type");
				Element literalElement = new Element("Literal", ogcNamespace).setText(SERIES);
				propertyIsLikeElement.addContent(propertyNameElement);
				propertyIsLikeElement.addContent(literalElement);
				
				Element propertyIsLikeElement2 = new Element("PropertyIsLike", ogcNamespace).setAttribute("singleChar", " ").setAttribute("escape", "\\\\").setAttribute("wildCard", "%");
				Element propertyNameElement2 = new Element("PropertyName", ogcNamespace).setText("AnyText");
				Element literalElement2 = new Element("Literal", ogcNamespace).setText(EXPERIMENTAL_SITE_LEVEL_NAME);
				propertyIsLikeElement2.addContent(propertyNameElement2);
				propertyIsLikeElement2.addContent(literalElement2);
				
				andElement.addContent(propertyIsLikeElement);
				andElement.addContent(propertyIsLikeElement2);
				orElement.addContent(andElement);
			}
			
			if (getCriteria().getIncludesDatasets())
			{
				Element propertyIsLikeElement = new Element("PropertyIsLike", ogcNamespace).setAttribute("singleChar", " ").setAttribute("escape", "\\\\").setAttribute("wildCard", "%");
				Element propertyNameElement = new Element("PropertyName", ogcNamespace).setText("Type");
				Element literalElement = new Element("Literal", ogcNamespace).setText(DATASET);
				propertyIsLikeElement.addContent(propertyNameElement);
				propertyIsLikeElement.addContent(literalElement);
				orElement.addContent(propertyIsLikeElement);
			}
			globalAndElement.addContent(orElement);
		}
		if ((getCriteria().getStartDate() != null) || (getCriteria().getEndDate() != null))
		{
			Element andElement = new Element("And", ogcNamespace);
			if (getCriteria().getStartDate() != null)
			{
				Element greaterElement = new Element("PropertyIsGreaterThanOrEqualTo", ogcNamespace);
				Element nameElement = new Element("PropertyName", ogcNamespace);
				nameElement.setText("tempExtentBegin");

				Element valueElement = new Element("Literal", ogcNamespace);
				valueElement.setText(getCriteria().getStartDate());

				greaterElement.addContent(nameElement);
				greaterElement.addContent(valueElement);

				andElement.addContent(greaterElement);
			}

			if (getCriteria().getEndDate() != null)
			{
				Element lesserElement = new Element("PropertyIsLessThanOrEqualTo", ogcNamespace);
				Element nameElement = new Element("PropertyName", ogcNamespace);
				nameElement.setText("tempExtentEnd");

				Element valueElement = new Element("Literal", ogcNamespace);
				valueElement.setText(getCriteria().getEndDate());

				lesserElement.addContent(nameElement);
				lesserElement.addContent(valueElement);

				andElement.addContent(lesserElement);
			}

			/*
			 * <Between> <PropertyName>SAMPLE_DATE</PropertyName>
			 * <LowerBoundary> <Literal>2001-01-15T20:07:48.11</Literal>
			 * </LowerBoundary> <UpperBoundary>
			 * <Literal>2001-03-06T12:00:00.00</Literal> </UpperBoundary>
			 * </Between>
			 * 
			 * <ogc:And> <ogc:PropertyIsGreaterThanOrEqualTo>
			 * <ogc:PropertyName>TempExtent_begin</ogc:PropertyName>
			 * <ogc:Literal>2010-10-01T01:01:01Z</ogc:Literal>
			 * </ogc:PropertyIsGreaterThanOrEqualTo>
			 * <ogc:PropertyIsLessThanOrEqualTo>
			 * <ogc:PropertyName>TempExtent_end</ogc:PropertyName>
			 * <ogc:Literal>2010-10-31T23:59:59Z</ogc:Literal>
			 * </ogc:PropertyIsLessThanOrEqualTo> </ogc:And>
			 */
			globalAndElement.addContent(andElement);

		}

		return getRecordsElement;
	}

	private boolean isValid(GeographicBoundingBox boundingBox)
	{
		if (Double.isNaN(boundingBox.getEastBoundLongitude()))
		{
			return false;
		}
		if (Double.isNaN(boundingBox.getWestBoundLongitude()))
		{
			return false;
		}
		if (Double.isNaN(boundingBox.getNorthBoundLatitude()))
		{
			return false;
		}
		if (Double.isNaN(boundingBox.getSouthBoundLatitude()))
		{
			return false;
		}
		return true;
	}

	public List<Summary> getSummaries()
	{
		return summaries;
	}

	public SearchCriteria getCriteria()
	{
		return criteria;
	}

	public void setCriteria(SearchCriteria criteria)
	{
		this.criteria = criteria;
	}

	public int getPagePosition()
	{
		return pagePosition;
	}

	public void setPagePosition(int pagePosition)
	{
		this.pagePosition = pagePosition;
	}

	public int getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public int getHits()
	{
		return hits;
	}

	@Override
	public boolean execute() throws Exception
	{
		throw new Exception();
	}

	public boolean isLoginNeeded()
	{
		return loginNeeded;
	}

	public void setLoginNeeded(boolean loginNeeded)
	{
		this.loginNeeded = loginNeeded;
	}

	public void setDisplayLanguages(ArrayList<String> displayLanguages) {
		this.displayLanguages = displayLanguages;
		
	}

}
