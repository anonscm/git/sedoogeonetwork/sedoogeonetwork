package fr.sedoo.rbv.geonetwork.request.csw;

public interface DCConstants {

	static final String DC_NAMESPACE="dc";
	static final String IDENTIFIER="identifier";
	static final String DATE="date";
	static final String TITLE="title";
	static final String DESCRIPTION="description";
	
}
