package fr.sedoo.rbv.geonetwork.request;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.jdom2.Document;
import org.jdom2.Element;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.commons.metadata.utils.tools.SummaryUnmarshaller;
import fr.sedoo.rbv.geonetwork.config.GeonetworkConfig;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

public class MySummaryRequest extends AbstractCSWRequest
{

	private List<Summary> summaries = new ArrayList<Summary>();

	public MySummaryRequest(GeonetworkUser user) 
	{
		super(user);
	}

	public MySummaryRequest(GeonetworkConfig config) 
	{
		super(config);
	}

	public MySummaryRequest() 
	{
		super();
	}

	@Override
	public boolean execute() throws Exception {


		Element request = getRequestElement();

		String cswURL = getConfig().getCSWPublicationURL();
		PostMethod post = new PostMethod(cswURL);

		try {
			String postData = getString(new Document(request));
			post.setRequestEntity(new StringRequestEntity(postData,"application/xml", UTF8));
			String rawXml = executePostMethod(post);
			if (rawXml.length()>0)
			{
				summaries = SummaryUnmarshaller.fromXML(rawXml);
				return true;
			}
			else
			{
				throw new Exception ("Empty response");
			}

		}
		catch (Exception e)
		{	
			throw new Exception(e);
		}
		finally {
			post.releaseConnection();
		}


	}

	private Element getRequestElement() {


		Element getRecords = new Element("GetRecords",cswNamespace).setAttribute("service", "CSW").setAttribute("version", "2.0.2").setAttribute("resultType", "results");
		Element query = new Element("Query", cswNamespace).setAttribute("typeNames","csw:Record");

		Element elementSetName = new Element("ElementSetName", cswNamespace).addContent("summary");

		Element constraint = new Element("Constraint", cswNamespace).setAttribute("version", "1.1.0");

		Element filter = new Element(OGC_FILTER_NODE_NAME, ogcNamespace);
		Element sortBy = new Element("SortBy", ogcNamespace);
		Element sortProperty = new Element("SortProperty", ogcNamespace);
		Element propertyName = new Element("PropertyName", ogcNamespace).addContent("date");
		Element sortOrder = new Element("SortOrder", ogcNamespace).addContent("ASC");

		getRecords.addContent(query);
		query.addContent(elementSetName);
		query.addContent(constraint);
		constraint.addContent(filter);
		query.addContent(sortBy);
		sortBy.addContent(sortProperty);
		sortProperty.addContent(propertyName);
		sortProperty.addContent(sortOrder);

		return getRecords;


	}

	public List<Summary> getSummaries() {
		return summaries;
	}

	public void setSummaries(List<Summary> summaries) {
		this.summaries = summaries;
	}


	/*
	public List<Summary> getSummaries()
	{

		boolean connectionResult = login();
		if (connectionResult == false)
		{
			return new ArrayList<Summary>();
		}
		else
		{

			Namespace csw = Namespace.getNamespace("csw", "http://www.opengis.net/cat/csw/2.0.2");
			Namespace ogc = Namespace.getNamespace("csw", "http://www.opengis.net/ogc");
			Namespace ogcWithoutPrefix = Namespace.getNamespace("http://www.opengis.net/ogc");

			Element getRecords = new Element("GetRecords",csw).setAttribute("service", "CSW").setAttribute("version", "2.0.2").setAttribute("resultType", "results").setAttribute("maxRecords",""+MAX_RETURNED_ROW_NUMBER);
			Element query = new Element("Query", csw).setAttribute("typeNames","csw:Record");

			Element elementSetName = new Element("ElementSetName", csw).addContent("summary");

			Element constraint = new Element("Constraint", csw).setAttribute("version", "1.1.0");

			Element filter = new Element("Filter", ogcWithoutPrefix);
			Element sortBy = new Element("SortBy", ogc);
			Element sortProperty = new Element("SortProperty", ogc);
			Element propertyName = new Element("PropertyName", ogc).addContent("date");
			Element sortOrder = new Element("SortOrder", ogc).addContent("ASC");


			getRecords.addContent(query);
			query.addContent(elementSetName);
			query.addContent(constraint);
			constraint.addContent(filter);
			query.addContent(sortBy);
			sortBy.addContent(sortProperty);
			sortProperty.addContent(propertyName);
			sortProperty.addContent(sortOrder);

			String cswURL = getConfig().getCSWURL();
			PostMethod post = new PostMethod(cswURL);


			try {
				String postData = getString(new Document(getRecords));
				System.out.println(postData);
				post.setRequestEntity(new StringRequestEntity(postData,"application/xml", "UTF8"));

				int result = httpclient.executeMethod(post);
				if (result == HttpStatus.SC_OK)
				{
					InputStream responseBodyAsStream = post.getResponseBodyAsStream();
					StringWriter writer = new StringWriter();
					IOUtils.copy(responseBodyAsStream, writer, "UTF8");
					String rawXml = writer.toString();
					return SummaryUnmarshaller.fromXML(rawXml);
				}
			}
			catch (Exception e)
			{	
				return new ArrayList<Summary>();
			}
			finally {
				post.releaseConnection();
			}
		}
		return new ArrayList<Summary>();
	}
	 */

}
