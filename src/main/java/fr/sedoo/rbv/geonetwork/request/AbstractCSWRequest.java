package fr.sedoo.rbv.geonetwork.request;

import org.jdom2.Element;
import org.jdom2.Namespace;

import fr.sedoo.rbv.geonetwork.config.GeonetworkConfig;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

/**
 * Classe abstraite pour les requete d'ajout/modification/suppression
 * @author andre
 *
 */
public abstract class AbstractCSWRequest extends AuthenticatedRequest{

	private static final String CSW_PREFIX = "csw";
	private static final String CSW_NAMESPACE = "http://www.opengis.net/cat/csw/2.0.2";
	protected final Namespace cswNamespace = Namespace.getNamespace(CSW_PREFIX, CSW_NAMESPACE);
	private static final String OGC_PREFIX = "ogc";
	private static final String OGC_NAMESPACE = "http://www.opengis.net/ogc";
	protected final Namespace ogcNamespace = Namespace.getNamespace(OGC_PREFIX, OGC_NAMESPACE);
	private static final String GMD_PREFIX = "gmd";
	private static final String GMD_NAMESPACE = "http://www.isotc211.org/2005/gmd";
	protected final Namespace gmdNamespace = Namespace.getNamespace(GMD_PREFIX, GMD_NAMESPACE);
	private static final String GCO_PREFIX = "gco";
	private static final String GCO_NAMESPACE = "http://www.isotc211.org/2005/gco";
	protected final Namespace gcoNamespace = Namespace.getNamespace(GCO_PREFIX, GCO_NAMESPACE);
	private static final String GML_PREFIX = "gml";
	private static final String GML_NAMESPACE = "http://www.opengis.net/gml";
	protected final Namespace gmlNamespace = Namespace.getNamespace(GML_PREFIX, GML_NAMESPACE);
	
	
	protected static final String OGC_FILTER_NODE_NAME = "Filter";
	
	public AbstractCSWRequest(GeonetworkConfig config) 
	{
		super(config);
	}

	public AbstractCSWRequest(GeonetworkUser user) {
		super(user);
	}
	
	public AbstractCSWRequest() {
		super();
	}

	public Element getCSWRootElement()
	{
		
		Element request = new Element("Transaction",cswNamespace); 
		request.setAttribute("service", "CSW").setAttribute("version", "2.0.2");
		return request;
	}
	
	public abstract boolean execute() throws Exception;
}
