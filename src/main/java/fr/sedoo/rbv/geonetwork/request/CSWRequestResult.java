package fr.sedoo.rbv.geonetwork.request;

import java.io.ByteArrayInputStream;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.binder.DigesterLoader;

public class CSWRequestResult 
{
	private Integer totalInserted = 0;
	private Integer totalUpdated = 0;
	private Integer totalDeleted = 0;
	private String identifier ="";
	
	public Integer getTotalInserted() {
		return totalInserted;
	}
	public void setTotalInserted(Integer totalInserted) {
		this.totalInserted = totalInserted;
	}
	public Integer getTotalUpdated() {
		return totalUpdated;
	}
	public void setTotalUpdated(Integer totalUpdated) {
		this.totalUpdated = totalUpdated;
	}
	public Integer getTotalDeleted() {
		return totalDeleted;
	}
	public void setTotalDeleted(Integer totalDeleted) {
		this.totalDeleted = totalDeleted;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	
	public static CSWRequestResult fromXML(String rawXml) throws Exception
	{
		DigesterLoader digesterLoader = DigesterLoader.newLoader(new CSWRequestResultModule());
		Digester digester = digesterLoader.newDigester();
        digester.setValidating(false);
        CSWRequestResult result = digester.parse(new ByteArrayInputStream(rawXml.getBytes()));
        return result;
	}
}
