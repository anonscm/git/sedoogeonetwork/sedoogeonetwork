package fr.sedoo.rbv.geonetwork.request;

import org.apache.commons.digester3.binder.AbstractRulesModule;

public class CSWRequestResultModule extends AbstractRulesModule {

	@Override
	public void configure() 
	{

		forPattern("csw:TransactionResponse").createObject().ofType(CSWRequestResult.class).then().setProperties();
		forPattern("*/"+"csw:totalInserted").setBeanProperty().withName("totalInserted");
		forPattern("*/"+"csw:totalDeleted").setBeanProperty().withName("totalDeleted");
		forPattern("*/"+"csw:totalUpdated").setBeanProperty().withName("totalUpdated");
		forPattern("*/"+"identifier").setBeanProperty().withName("identifier");
	}

}
