package fr.sedoo.rbv.geonetwork.request;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.jdom2.Document;
import org.jdom2.Element;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkGroup;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.xml.GroupUnmarshaller;

public class GroupRequest extends AuthenticatedRequest
{

	public GroupRequest(GeonetworkUser user) 
	{
		super(user);
	}
	
	public GroupRequest() 
	{
		super();
	}
	
	public List<GeonetworkGroup> execute() throws Exception
	{
			Element request = new Element("request");
			String userListURL = getConfig().getGroupListURL();
			//String userListURL = getConfig().getAdminURL();
			PostMethod post = new PostMethod(userListURL);
			String postData = getString(new Document(request));
			post.setRequestEntity(new StringRequestEntity(postData,"application/xml", "UTF8"));
			String rawXml = executePostMethod(post);
			return GroupUnmarshaller.fromXML(rawXml);
	}

}
