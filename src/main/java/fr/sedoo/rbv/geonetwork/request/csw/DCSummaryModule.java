package fr.sedoo.rbv.geonetwork.request.csw;

import java.util.ArrayList;

import org.apache.commons.digester3.binder.AbstractRulesModule;

import fr.sedoo.commons.metadata.utils.domain.Summary;

public class DCSummaryModule extends AbstractRulesModule implements DCConstants, CSWConstants
{

	@Override
	public void configure()
	{

		forPattern(CSW_NAMESPACE + ":GetRecordsResponse").createObject().ofType(ArrayList.class).then().setProperties();
		forPattern("*/" + CSW_NAMESPACE + ":SummaryRecord").createObject().ofType(Summary.class).then().setProperties().then().setNext("add");
		forPattern("*/" + DC_NAMESPACE + ":" + IDENTIFIER).setBeanProperty().withName("uuid");

	}

}
