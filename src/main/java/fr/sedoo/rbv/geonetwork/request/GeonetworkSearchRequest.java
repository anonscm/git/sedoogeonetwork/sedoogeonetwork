package fr.sedoo.rbv.geonetwork.request;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.jdom2.Document;
import org.jdom2.Element;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkHarvester;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkMetadataInfo;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.xml.GeonetworkMetadataInfoUnmarshaller;
import fr.sedoo.rbv.geonetwork.xml.HarvesterUnmarshaller;

public class GeonetworkSearchRequest extends AuthenticatedRequest
{

	private String observatoryName;
	private boolean success = false;
	private ArrayList<String> uuids;

	public GeonetworkSearchRequest(GeonetworkUser user ) 
	{
		super(user);
	}

	public GeonetworkSearchRequest() 
	{
		super();
	}

	public void execute() throws Exception
	{
		String harvesterUuid = getHarvesterUuidFromObservatoryName();
		getHarvestedEntries(harvesterUuid);
	}

	private void getHarvestedEntries(String harvesterUuid) throws Exception{

		uuids = new ArrayList<String>();
		String url = getConfig().getSearchURL();
		Element request = new Element("request");

		PostMethod post = new PostMethod(url);

		try {
			String postData = getString(new Document(request));
			post.setRequestEntity(new StringRequestEntity(postData,"application/xml", "UTF8"));

			String rawXml = executePostMethod(post);
			List<GeonetworkMetadataInfo> tmp = GeonetworkMetadataInfoUnmarshaller.fromXML(rawXml);
			Iterator<GeonetworkMetadataInfo> iterator = tmp.iterator();
			while (iterator.hasNext()) 
			{
				GeonetworkMetadataInfo geonetworkMetadataInfo = iterator.next();
				if (geonetworkMetadataInfo.getHarvesterUuid().compareToIgnoreCase(harvesterUuid)==0)
				{
					uuids.add(geonetworkMetadataInfo.getMetadataUuid());
				}
			}

			return;
		}
		catch (Exception e)
		{	
			throw e;
		}
		finally {
			post.releaseConnection();
		}
	}

	public String getHarvesterUuidFromObservatoryName() throws Exception
	{

		Element request = new Element("request");
		String userListURL = getConfig().getHarvesterListURL();
		PostMethod post = new PostMethod(userListURL);

		try {
			String postData = getString(new Document(request));
			post.setRequestEntity(new StringRequestEntity(postData,"application/xml", "UTF8"));
			String rawXml = executePostMethod(post);
			List<GeonetworkHarvester> list = HarvesterUnmarshaller.fromXML(rawXml);
			Iterator<GeonetworkHarvester> iterator = list.iterator();
			while (iterator.hasNext()) {
				GeonetworkHarvester geonetworkHarvester = (GeonetworkHarvester) iterator.next();
				if (geonetworkHarvester.getName().toLowerCase().contains(observatoryName.toLowerCase()))
				{
					return geonetworkHarvester.getUuid();
				}
			}
			throw new Exception("No harvester");
		}
		catch (Exception e)
		{	
			throw e;
		}
		finally {
			post.releaseConnection();
		}
	}


	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getObservatoryName() {
		return observatoryName;
	}

	public void setObservatoryName(String observatoryName) {
		this.observatoryName = observatoryName;
	}

	public ArrayList<String> getUuids() {
		return uuids;
	}

	public void setUuids(ArrayList<String> uuids) {
		this.uuids = uuids;
	}

}
