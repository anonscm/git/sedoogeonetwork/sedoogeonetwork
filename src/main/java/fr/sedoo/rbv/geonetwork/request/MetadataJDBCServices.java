package fr.sedoo.rbv.geonetwork.request;

import org.springframework.jdbc.core.JdbcTemplate;

import fr.sedoo.rbv.geonetwork.config.GeonetworkConfig;
import fr.sedoo.rbv.geonetwork.dao.GeonetworkGroup;
import fr.sedoo.rbv.geonetwork.dao.GroupDAO;
import fr.sedoo.rbv.geonetwork.dao.JDBCGroupDAO;

public class MetadataJDBCServices {

	public static Long allGroupId = null;
	
	public static void setPublic(String uuid, AuthenticatedRequest request) throws Exception 
	{
		GeonetworkConfig config = request.getConfig();
		if (allGroupId == null)
		{
			initAllGroupId(config);
		}
		
		Long id = getIdFromUuid(uuid, config);
		MakePublicRequest makePublicRequest = new MakePublicRequest(request.getUser(), id, allGroupId);
		try
		{
			boolean result = makePublicRequest.execute();
			if (result == false)
			{
				throw new Exception();
			}
		}
		catch (Exception e)
		{
			throw new Exception("Unable to set the metadata publicly available");
		}
	}
	
	private static void initAllGroupId(GeonetworkConfig config)
	{
//		JDBCGroupDAO groupDAO = new JDBCGroupDAO();
//		groupDAO.setDataSource(config.getDataSource());
//		GeonetworkGroup allGroup = groupDAO.getGroupByName(GroupDAO.ALL_GROUP_NAME);
//		allGroupId = allGroup.getId();
	}
	
	public static void setHarvested(String uuid, Boolean isHarvested, GeonetworkConfig config)
	{
//		String textValue="n";
//		
//		if (isHarvested)
//		{
//			textValue="y";
//		}
//		
//		String sqlRequest = "update metadata set isharvested=? where uuid= ? ";
//		JdbcTemplate template = new JdbcTemplate(config.getDataSource());
//		template.update(sqlRequest, textValue, uuid);
	}
	
	private static Long getIdFromUuid(String uuid, GeonetworkConfig config)
	{
//		String sqlRequest = "select id from metadata where uuid= ?";
//		JdbcTemplate template = new JdbcTemplate(config.getDataSource());
//		return (Long) template.queryForLong(sqlRequest, new Object[] {uuid});
		return 0L;
	}


}
