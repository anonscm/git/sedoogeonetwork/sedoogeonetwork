package fr.sedoo.rbv.geonetwork.request;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.jdom2.Document;
import org.jdom2.Element;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkGroup;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

public class PrivilegeUdpateRequest extends AuthenticatedRequest
{

	private GeonetworkUser targetUser;
	private List<String> uuids;
	private boolean success = false;

	public PrivilegeUdpateRequest(GeonetworkUser user ) 
	{
		super(user);
	}

	public PrivilegeUdpateRequest() 
	{
		super();
	}


	public void execute() throws Exception
	{
		GroupRequest groupRequest = new GroupRequest();
		List<GeonetworkGroup> groups = groupRequest.execute();
		String groupId = getGroupIdByUserName(groups, targetUser);
		Iterator<String> iterator = uuids.iterator();
		while (iterator.hasNext()) 
		{
			boolean success = executeSingleUpdate(iterator.next(), groupId);
			if (!success)
			{
				this.setSuccess(success);
				return;
			}
		}
		this.setSuccess(true);
	}

	public boolean executeSingleUpdate(String uuid, String groupId)
	{
		Element request = new Element("request");
		Element id = new Element("uuid");
		id.setText(uuid);
		request.addContent(id);
		request.addContent(new Element("_"+groupId+"_"+0));
		request.addContent(new Element("_"+groupId+"_"+1));
		request.addContent(new Element("_"+groupId+"_"+2));
		request.addContent(new Element("_"+groupId+"_"+3));
		request.addContent(new Element("_"+groupId+"_"+5));

		String metadataPrivilegeURL = getConfig().getPrivilegeURL();
		PostMethod post = new PostMethod(metadataPrivilegeURL);

		try {
			String postData = getString(new Document(request));
			post.setRequestEntity(new StringRequestEntity(postData,"application/xml", "UTF8"));
			String rawXml = executePostMethod(post);
			if (rawXml.indexOf("rror")>0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		catch (Exception e)
		{	
			return false;
		}
		finally {
			post.releaseConnection();
		}
	}

	private String getGroupIdByUserName(List<GeonetworkGroup> groups, GeonetworkUser targetUser) 
	{
		Iterator<GeonetworkGroup> iterator = groups.iterator();
		while (iterator.hasNext()) 
		{
			GeonetworkGroup current = (GeonetworkGroup) iterator.next();
			if (current.getName().compareToIgnoreCase(targetUser.getLogin())==0)
			{
				return current.getId();
			}
		}
		return null;
	}

	public GeonetworkUser getTargetUser() {
		return targetUser;
	}

	public void setTargetUser(GeonetworkUser targetUser) {
		this.targetUser = targetUser;
	}

	public List<String> getUuids() {
		return uuids;
	}

	public void setUuids(List<String> uuids) {
		this.uuids = uuids;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
