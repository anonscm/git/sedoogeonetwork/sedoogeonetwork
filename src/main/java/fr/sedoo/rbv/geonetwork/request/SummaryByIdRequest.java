package fr.sedoo.rbv.geonetwork.request;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.binder.DigesterLoader;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.xml.sax.InputSource;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.rbv.geonetwork.config.GeonetworkConfig;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.csw.SummaryModule;

public class SummaryByIdRequest extends AbstractCSWRequest
{

	private static final String LOCALE_ATTRIBUTE = "#locale-";
	/**
	 * Iso indicates that language french is to be "fre" but "fra" is used instead...
	 */
	public static final String LOCALE_SRC_FRENCH_ATTRIBUTE = "#locale-fra";
	public static final String LOCALE_TGT_FRENCH_ATTRIBUTE = "#locale-fre";
	private List<Summary> summaries = new ArrayList<Summary>();
	private List<String> ids = new ArrayList<String>();
	private ArrayList<String> displayLanguages = new ArrayList<String>();

	public SummaryByIdRequest(GeonetworkUser user)
	{
		super(user);
	}

	public SummaryByIdRequest(GeonetworkConfig config)
	{
		super(config);
	}

	public SummaryByIdRequest()
	{
		super();
	}

	public void setDisplayLanguages(ArrayList<String> displayLanguages) {
		this.displayLanguages = displayLanguages;
	}

	@Override
	public boolean execute() throws Exception
	{
		if (getIds().isEmpty())
		{
			//Ce cas peut survenir lors de l'utilisation du quick search
			summaries = new ArrayList<Summary>();
			return true;
			//throw new Exception("Empty id list");
		}

		Element request = getGetRecordByIdElement();
		String cswURL = getConfig().getCSWPublicationURL();
		PostMethod post = new PostMethod(cswURL);

		try
		{
			String postData = getString(new Document(request));
			post.setRequestEntity(new StringRequestEntity(postData, "application/xml", UTF8));

			String rawXml = executePostMethod(post);
			if (rawXml.length() > 0)
			{
				rawXml = adaptTranslationTag(rawXml, displayLanguages);
				//System.out.println(rawXml);
				DigesterLoader digesterLoader = DigesterLoader.newLoader(new SummaryModule());
				Digester digester = digesterLoader.newDigester();
				digester.setValidating(false);

				ByteArrayInputStream inputStream = new ByteArrayInputStream(rawXml.getBytes());
				Reader reader = new InputStreamReader(inputStream,"UTF-8");
				InputSource is = new InputSource(reader);
				is.setEncoding("UTF-8");

				List<Summary> tmp = digester.parse(is);

				// La liste n'est pas forc�ment dans l'ordre initial des
				// ids. On reconstruit celui-ci.

				HashMap<String, Summary> tmpMap = new HashMap<String, Summary>();
				Iterator<Summary> iterator = tmp.iterator();
				while (iterator.hasNext())
				{
					Summary summary = iterator.next();
					tmpMap.put(summary.getUuid(), summary);
				}

				List<Summary> sortedList = new ArrayList<Summary>();

				Iterator<String> idIterator = getIds().iterator();
				while (idIterator.hasNext())
				{
					String currentId = idIterator.next();
					sortedList.add(tmpMap.get(currentId));
				}
				summaries = sortedList;
				return true;
			} else
			{
				throw new Exception();
			}
		} catch (Exception e)
		{
			throw new Exception(e);
		} finally
		{
			post.releaseConnection();
		}

	}

	/*
	 * private Element getGetRecordByIdElement() {
	 * 
	 * 
	 * Element element = new
	 * Element("GetRecordById",cswNamespace).setAttribute("service",
	 * "CSW").setAttribute("version", "2.0.2"); element.addContent(new
	 * Element("ElementSetName", cswNamespace).setText("full"));
	 * Iterator<String> iterator = getIds().iterator(); while
	 * (iterator.hasNext()) { String uuid = iterator.next();
	 * element.addContent(new Element("Id",cswNamespace).setText(uuid));
	 * 
	 * 
	 * } return element; }
	 */

	/**
	 * Le digester n'interprete pas les expression XPath mettant en oeuvre les attributs.
	 * Cette fonction effectue une transformation rapide (bas�e sur des remplacement textuels simples) remplacant attributs par noeuds.
	 * @param rawXml
	 * @return
	 */

	public static String adaptTranslationTag(String xml, List<String> languages)
	{
		String aux = xml;
		//We correct the fra->fre problem
		aux = aux.replace(LOCALE_SRC_FRENCH_ATTRIBUTE, LOCALE_TGT_FRENCH_ATTRIBUTE);
		Iterator<String> iterator = languages.iterator();
		while (iterator.hasNext()) 
		{
			aux = adaptTranslationTag(aux, iterator.next());
		}
		return aux;
	}

	private static String adaptTranslationTag(String aux, String language) {
		String languageAttribute=LOCALE_ATTRIBUTE+language;
		StringBuilder sb = new StringBuilder(aux);
		int lastIndex=0;
		while (lastIndex >=0)
		{
			lastIndex = sb.indexOf(languageAttribute, lastIndex+1);
			if (lastIndex>=0)
			{
				int i = sb.indexOf(">", lastIndex);
				sb.insert(i+1,"<" + SummaryModule.LANG_TAG + ">"+language.toLowerCase()+"</" + SummaryModule.LANG_TAG + "><" + SummaryModule.VALUE_TAG + ">");
				i = sb.indexOf("<" + SummaryModule.VALUE_TAG + ">", i);
				i = sb.indexOf("</", i);
				sb.insert(i,"</" + SummaryModule.VALUE_TAG + ">");
			}
		}
		return sb.toString();
	}

	private Element getGetRecordByIdElement()
	{
		Element getRecords = new Element("GetRecords", cswNamespace).setAttribute("service", "CSW").setAttribute("version", "2.0.2").setAttribute("resultType", "results")
				.setAttribute("outputSchema", "csw:IsoRecord").setAttribute("maxRecords", "" + getIds().size());
		Element query = new Element("Query", cswNamespace).setAttribute("typeNames", "gmd:MD_Metadata").setAttribute("elementnameStrategy", "context");
		getRecords.addContent(query);

		Element global = new Element("ElementName", cswNamespace).setText("/gmd:MD_Metadata");
		query.addContent(global);
		/*
		 * Element dateStamp = new Element("ElementName",
		 * cswNamespace).setText("/gmd:MD_Metadata/gmd:dateStamp");
		 * 
		 * Element fileIdentifier = new Element("ElementName",
		 * cswNamespace).setText("/gmd:MD_Metadata/gmd:fileIdentifier"); Element
		 * dataIdentification = new Element("ElementName",
		 * cswNamespace).setText(
		 * "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification");
		 * Element observatoryName = new Element("ElementName",
		 * cswNamespace).setText(
		 * "/gmd:MD_Metadata/rbv:rbvExtent/rbv:localisationExtent/rbv:observatoryName"
		 * );
		 * 
		 * query.addContent(dateStamp); query.addContent(fileIdentifier);
		 * query.addContent(dataIdentification);
		 * query.addContent(observatoryName);
		 */

		Element constraint = new Element("Constraint", cswNamespace).setAttribute("version", "1.1.0");
		Element filter = new Element("Filter", ogcNamespace);
		Element or = new Element("Or", ogcNamespace);

		query.addContent(constraint);
		constraint.addContent(filter);
		filter.addContent(or);

		Iterator<String> iterator = getIds().iterator();
		while (iterator.hasNext())
		{
			String current = iterator.next();

			Element propertyIsEqualTo = new Element("PropertyIsEqualTo", ogcNamespace);
			Element propertyName = new Element("PropertyName", ogcNamespace).setText("dc:identifier");
			Element propertyValue = new Element("Literal", ogcNamespace).setText(current);

			or.addContent(propertyIsEqualTo);
			propertyIsEqualTo.addContent(propertyName);
			propertyIsEqualTo.addContent(propertyValue);

		}

		return getRecords;
	}

	/*
	 * 
	 * <csw:Query typeNames="gmd:MD_Metadata"
	 * elementnameStrategy="geonetwork26"> <!-- xpaths must start with / (ie.
	 * they are measured from the root element) and can only contain '/', '[]',
	 * and ::node --> <!-- note: if the xpath does not apply to the metadata
	 * record matched or there are no elements that match the xpath then an
	 * empty element will be returned -->
	 * <csw:ElementName>/gmd:MD_Metadata/gmd:dateStamp</csw:ElementName>
	 * <csw:ElementName>/gmd:MD_Metadata/gmd:fileIdentifier</csw:ElementName>
	 * <csw:ElementName>/gmd:MD_Metadata/gmd:identificationInfo/gmd:
	 * MD_DataIdentification</csw:ElementName>
	 * <csw:ElementName>/gmd:MD_Metadata/
	 * rbv:rbvExtent/rbv:localisationExtent/rbv
	 * :observatoryName</csw:ElementName>
	 */

	public List<Summary> getSummaries()
	{
		return summaries;
	}

	public void setSummaries(List<Summary> summaries)
	{
		this.summaries = summaries;
	}

	public List<String> getIds()
	{
		return ids;
	}

	public void setIds(List<String> ids)
	{
		this.ids = ids;
	}

}
