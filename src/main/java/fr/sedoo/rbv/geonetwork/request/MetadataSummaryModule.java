package fr.sedoo.rbv.geonetwork.request;

import org.apache.commons.digester3.binder.AbstractRulesModule;

public class MetadataSummaryModule extends AbstractRulesModule {

	@Override
	public void configure() 
	{
		forPattern("gmd:MD_Metadata").createObject().ofType(MetadataSummary.class).then().setProperties();
		forPattern("*/"+"gmd:fileIdentifier/gco:CharacterString").setBeanProperty().withName("uuid");
		forPattern("*/"+"gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString").setBeanProperty().withName("resourceTitle");
	}

}
