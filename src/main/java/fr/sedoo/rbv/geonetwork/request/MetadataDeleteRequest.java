package fr.sedoo.rbv.geonetwork.request;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.jdom2.Document;
import org.jdom2.Element;

import fr.sedoo.rbv.geonetwork.config.GeonetworkConfig;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

public class MetadataDeleteRequest extends AbstractCSWRequest 
{

	private static final String DC_IDENTIFIER = "dc:identifier";
	private static final String CSW_DELETE_NODE_NAME = "Delete";
	private static final String CSW_CONSTRAINT_NODE_NAME = "Constraint";
	private static final String CSW_VERSION_ATTR_NAME = "version";
	private static final String METADATA_UUID_KEY = "metadata_uuid_key";
	private static final String EXCEPTION_REPORT = "ExceptionReport";
	private static final String OGC_PROPERTY_IS_EQUAL_TO_NODE_NAME = "PropertyIsEqualTo";
	private static final String OGC_PROPERTY_NAME_NODE_NAME = "PropertyName";
	private static final String OGC_LITERAL_NODE_NAME = "Literal";

	String id="";

	public MetadataDeleteRequest(String id, GeonetworkConfig config)
	{
		super(config);
		this.id = id;

	}

	public MetadataDeleteRequest(String id, GeonetworkUser user)
	{
		super(user);
		this.id = id;

	}

	public MetadataDeleteRequest(String id)
	{
		super();
		this.id = id;

	}

	public boolean execute() throws Exception
	{

		/*
		 * On utilise une requete CSW avec un outputschema par défaut et un ElementSetName de type brief, afin de ne ramener que les informations minimales 
		 */

		Element request = getCSWRootElement();
		request.addContent(getDeleteElement());

		String cswURL = getConfig().getCSWPublicationURL();
		PostMethod post = new PostMethod(cswURL);

		try {
			String postData = getString(new Document(request));
			post.setRequestEntity(new StringRequestEntity(postData,"application/xml", UTF8));
			String rawXml = executePostMethod(post);
			if (rawXml.length()>0)
			{
				if (rawXml.contains(EXCEPTION_REPORT))
				{
					if (rawXml.contains(METADATA_UUID_KEY))
					{
						throw new Exception("Existing key - "+rawXml);
					}
					else
					{
						throw new Exception(rawXml);
					}
				}
				else
				{
					CSWRequestResult requestResult = CSWRequestResult.fromXML(rawXml);
					if (requestResult.getTotalDeleted() == 1)
					{
						return true;
					}
					else
					{
						throw new Exception("The deletion hasn't been performed");
					}
				}
			}
			else
			{
				throw new Exception ();
			}
		}
		catch (Exception e)
		{	
			throw new Exception(e);
		}
		finally {
			post.releaseConnection();
		}

	}

	private Element getDeleteElement() {
		Element deleteElement = new Element(CSW_DELETE_NODE_NAME,cswNamespace);
		Element constraintElement = new Element(CSW_CONSTRAINT_NODE_NAME,cswNamespace);
		constraintElement.setAttribute(CSW_VERSION_ATTR_NAME, "1.1.0");
		deleteElement.addContent(constraintElement);

		Element filterElement = new Element(OGC_FILTER_NODE_NAME, ogcNamespace);
		constraintElement.addContent(filterElement);

		Element propertyIsEqualToElement = new Element(OGC_PROPERTY_IS_EQUAL_TO_NODE_NAME, ogcNamespace);
		filterElement.addContent(propertyIsEqualToElement);

		Element propertyNameElement = new Element(OGC_PROPERTY_NAME_NODE_NAME, ogcNamespace);
		propertyNameElement.setText(DC_IDENTIFIER);
		propertyIsEqualToElement.addContent(propertyNameElement);

		Element literalElement = new Element(OGC_LITERAL_NODE_NAME, ogcNamespace);
		literalElement.setText(id);
		propertyIsEqualToElement.addContent(literalElement);

		return deleteElement;
	}
}
