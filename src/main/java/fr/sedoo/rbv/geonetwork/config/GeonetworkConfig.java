package fr.sedoo.rbv.geonetwork.config;

import javax.sql.DataSource;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;

public class GeonetworkConfig 
{
	
	private String rootURL;
	private String adminLogin;
	private String adminPassword;
	
	
	public String getLoginURL()
	{
		return rootURL+"srv/eng/j_spring_security_check";
	}
	
	public String getRootURL() {
		return rootURL;
	}

	public void setRootURL(String rootURL) {
		this.rootURL = rootURL;
	}

	public String getCSWURL() {
		return rootURL+"srv/eng/csw";
	}


	public String getUserListURL() {
		return rootURL+"srv/eng/xml.user.list";
	}
	
	public String getGroupListURL() {
		return rootURL+"srv/eng/xml.group.list";
	}

	public String getRssURL() {
		return rootURL+"srv/eng/rss.search?sortBy=changeDate";
	}
	
	public String getSearchURL() {
		return rootURL+"srv/eng/xml.search";
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public String getAdminLogin() {
		return adminLogin;
	}

	public void setAdminLogin(String adminLogin) {
		this.adminLogin = adminLogin;
	}

	public GeonetworkUser getAdminUser() 
	{
		GeonetworkUser user = new GeonetworkUser();
		user.setLogin(getAdminLogin());
		user.setPassword(getAdminPassword());
		return user;
	}

	public String getCSWPublicationURL() {
		return rootURL+"srv/eng/csw-publication";
	}

	public String getPrivilegeURL() {
		return rootURL+"srv/eng/metadata.admin";
		
	}

	public String getSuggestURL() {
		return rootURL+"srv/eng/main.search.suggest?field=";
	}

	public String getHarvesterListURL() {
		return rootURL+"srv/eng/xml.harvesting.get";
	}
	
	public String getHarvesterActivateURL() {
		return rootURL+"srv/eng/xml.harvesting.start";
	}
	
	public String getHarvesterDesactivateURL() {
		return rootURL+"srv/eng/xml.harvesting.stop";
	}
	
	public String getHarvesterRunURL() {
		return rootURL+"srv/eng/xml.harvesting.run";
	}

	public String getLoginCheckURL() {
		return rootURL+"srv/eng/xml.info?type=me";
	}

	public String getAdminURL() {
		return rootURL+"srv/eng/admin";
	}

}
