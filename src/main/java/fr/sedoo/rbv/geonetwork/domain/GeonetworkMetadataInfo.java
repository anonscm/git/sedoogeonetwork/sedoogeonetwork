package fr.sedoo.rbv.geonetwork.domain;

public class GeonetworkMetadataInfo {

	private String metadataUuid;
	private String harvesterUuid;
	
	public String getMetadataUuid() {
		return metadataUuid;
	}
	public void setMetadataUuid(String metadataUuid) {
		this.metadataUuid = metadataUuid;
	}
	public String getHarvesterUuid() {
		return harvesterUuid;
	}
	public void setHarvesterUuid(String harvesterUuid) {
		this.harvesterUuid = harvesterUuid;
	}
}
