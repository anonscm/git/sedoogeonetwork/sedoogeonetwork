package fr.sedoo.rbv.geonetwork.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.UserRequest;

public class GeonetworkUserDAOImpl implements GeonetworkUserDAO{

	private Map<String,GeonetworkUser> users;
	
	private String login;
	private String password;
	
	public List<GeonetworkUser> findAll() {
	
		if (users == null)
		{
			initUsers();
		}
		return new ArrayList<GeonetworkUser>(users.values());
	}

	public GeonetworkUser findUserByLogin(String login) {
		
		if (users == null)
		{
			initUsers();
		}
		return users.get(login);
	}

	private void initUsers() 
	{
		users = new HashMap<String, GeonetworkUser>();
		GeonetworkUser connectionUser = new GeonetworkUser();
		connectionUser.setLogin(login);
		connectionUser.setPassword(password);
		UserRequest request = new UserRequest(connectionUser);
		List<GeonetworkUser> aux = request.execute();
		Iterator<GeonetworkUser> iterator = aux.iterator();
		while (iterator.hasNext()) {
			GeonetworkUser user = iterator.next();
			users.put(user.getLogin(), user);
		}
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

}
