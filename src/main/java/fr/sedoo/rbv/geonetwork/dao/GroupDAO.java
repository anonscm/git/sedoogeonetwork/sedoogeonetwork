package fr.sedoo.rbv.geonetwork.dao;

import java.util.List;

public interface GroupDAO {
	
	final static String BEAN_NAME = "groupDAO";
	final static String ALL_GROUP_NAME = "all";
	
	List<GeonetworkGroup> getGroups();
	
	GeonetworkGroup getGroupByName(String name);

}
