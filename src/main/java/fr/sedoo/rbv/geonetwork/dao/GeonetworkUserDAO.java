package fr.sedoo.rbv.geonetwork.dao;

import java.util.List;

import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;


public interface GeonetworkUserDAO {
	
	List<GeonetworkUser> findAll();
	GeonetworkUser findUserByLogin(String login);

}
